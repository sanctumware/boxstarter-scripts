# Description

Prepares a personal working environment.  It provides the following:

 * General Applications (PDF Reader, Text Editor, etc)
 * Media applications
 * Personal cloud services (Dropbox)
 * Common helper tools

## Boxstarter Scripts

Use these provided installation configuration file with [Boxstarter](http://boxstarter.org/) and its [web launcher technique](http://boxstarter.org/WebLauncher).

### Usage

		http://boxstarter.org/package/nr/url?https://raw.githubusercontent.com/jrbeverly/boxstarter-scripts/master/Personal/install.bat
