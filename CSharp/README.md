# Description

Prepares a development environment for CSharp.  It provides the following:

 * General Applications (PDF Reader, Text Editor, etc)
 * Git-Oriented Versioning Software (Github / Sourcetree)
 * Visual Studio IDE 
 * Common helper development tools

## Boxstarter Scripts

Use these provided installation configuration file with [Boxstarter](http://boxstarter.org/) and its [web launcher technique](http://boxstarter.org/WebLauncher).

## How to use

		http://boxstarter.org/package/nr/url?https://raw.githubusercontent.com/jrbeverly/boxstarter-scripts/master/CSharp/install.bat
