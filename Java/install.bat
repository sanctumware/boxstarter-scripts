# -------------------------------------------------
# Windows configuration changes
# -------------------------------------------------

Disable-InternetExplorerESC
Disable-UAC
Enable-RemoteDesktop
Set-ExplorerOptions -showHidenFilesFoldersDrives -showProtectedOSFiles -showFileExtensions
Set-TaskbarOptions -Size Small -Lock -Dock Bottom
Update-ExecutionPolicy Unrestricted

# -------------------------------------------------
# General Programs
# -------------------------------------------------

choco install foxitreader -y
choco install ccleaner -y
choco install notepadplusplus -y
choco install nimbletext -y
choco install 7zip.install - y

# -------------------------------------------------
# Web Browsers
# -------------------------------------------------

choco install Firefox -y
choco install GoogleChrome -y

# -------------------------------------------------
# Git Software
# -------------------------------------------------

choco install git -y
choco install github -y
choco install sourcetree -y

# -------------------------------------------------
# Java Developer Software
# -------------------------------------------------

choco install javaruntime -y
choco install jdk8 -y

# -------------------------------------------------
# Developer Software
# -------------------------------------------------

choco install markdownpad2 -y
choco install eclipse -y
choco install putty.install -y
choco install winmerge -y
