# Boxstarter Scripts

A collection of scripts designed to quickly setup a Windows based environment.  Each directory contains an environment, with instructions of how to install the specific development version.

## Description

| Name      | Description |
|-------    |-------------|
| Personal      | Prepares a personal working environment |
| Full      | Prepares a standard working environment |
| Partial   | Prepares a standard working environment with bare essentials |
| CSharp    | Prepares a development environment for CSharp |
| Java      | Prepares a development environment for Java |