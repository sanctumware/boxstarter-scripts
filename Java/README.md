# Description

Prepares a development environment for Java.  It provides the following:

 * General Applications (PDF Reader, Text Editor, etc)
 * Git-Oriented Versioning Software (Github / Sourcetree)
 * Eclipse IDE & Java SDK
 * Common helper development tools

## Boxstarter Scripts

Use these provided installation configuration file with [Boxstarter](http://boxstarter.org/) and its [web launcher technique](http://boxstarter.org/WebLauncher).

### Usage

		http://boxstarter.org/package/nr/url?https://raw.githubusercontent.com/jrbeverly/boxstarter-scripts/master/Java/install.bat
