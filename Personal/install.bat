# -------------------------------------------------
# Windows configuration changes
# -------------------------------------------------

Disable-InternetExplorerESC
Disable-UAC
Enable-RemoteDesktop
Set-ExplorerOptions -showHidenFilesFoldersDrives -showProtectedOSFiles -showFileExtensions
Set-TaskbarOptions -Size Small -Lock -Dock Bottom
Update-ExecutionPolicy Unrestricted

# -------------------------------------------------
# General Programs
# -------------------------------------------------

choco install javaruntime -y
choco install foxitreader -y
choco install ccleaner -y
choco install 7zip.install - y
choco install vlc -y

# -------------------------------------------------
# Web Programs
# -------------------------------------------------

choco install GoogleChrome -y

# -------------------------------------------------
# Cloud Services
# -------------------------------------------------

choco install dropbox -y
choco install googledrive -y

# -------------------------------------------------
# Developer Software
# -------------------------------------------------

choco install virtualbox -y
choco install putty.install -y

